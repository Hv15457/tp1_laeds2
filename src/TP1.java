import java.util.ArrayList;
import java.util.Scanner;

public class TP1{
    public static void main(String[] args) {
        ArvorePatricia dicionario = new ArvorePatricia (8); // instancia nova arvore com 8 bits representativos
        Scanner sc = new Scanner(System.in);
        String palavra;
        indexaTexto(dicionario);
        palavra = sc.nextLine();
        for(char c : palavra.toCharArray()){ // que
            dicionario.pesquisa(c);
        } // tem que terminar que fazer a busca
    }

    public static void indexaTexto(ArvorePatricia dicionario){
        int coluna = 0;
        int linha = 0;
        String textoASerIndexado = "../docs/exemplo1.txt"; // nome do arquivo de texto a ser passado como parametro para ExtraiPalavra
        ExtraiPalavra texto = new ExtraiPalavra ("delim.txt", textoASerIndexado);
        ArrayList<MeuItem> palavras = new ArrayList<MeuItem>();
        //comecando a insercao
        do{
            MeuItem palavra = new MeuItem(); // palavra atual
            String token = texto.proximaPalavra(); // pega a string da palavra
            palavra.setPalavra(token); // insere a string no objeto palavra
            palavra.setLinha(texto.getLinhaAtual()); // insere a linha no objeto palavra
            palavra.setColuna(texto.getContaColunas()); // insere a coluna no objeto palavra
            for(int j=0;j<palavra.getPalavra().length();j++) // insere cada caractere da string na arvore
                dicionario.insere(palavra.getPalavra().charAt(i));
            palavras.add(palavra); // insere o objeto com a string no array de objetos
        }while (palavra!=null);
        texto.fecharArquivos();
    }
}