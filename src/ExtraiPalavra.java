import java.util.StringTokenizer;
import java.io.*;
public class ExtraiPalavra {
  private BufferedReader arqDelim, arqTxt;
  private StringTokenizer palavras;
  private String delimitadores;
  private int linhaAtual=0;
  private int contaColunas=1;

  public ExtraiPalavra (String nomeArqDelim, String nomeArqTxt) 
  throws Exception {
    this.arqDelim = new BufferedReader (new FileReader (nomeArqDelim));
    this.arqTxt = new BufferedReader (new FileReader (nomeArqTxt));
    // @{\it Os delimitadores devem estar juntos em uma \'unica linha do arquivo}@ 
    this.delimitadores = arqDelim.readLine() + "\r\n"; 
    this.palavras = null;
  }  
  public String proximaPalavra () throws Exception{
    if (palavras == null || !palavras.hasMoreTokens()) {
      String linha = arqTxt.readLine();
      contaColunas=1;
      linhaAtual++;
      if (linha == null) return null; 
      this.palavras = new StringTokenizer (linha, this.delimitadores);
      if (!palavras.hasMoreTokens()) return ""; // @{\it ignora delimitadores}@
    }
    contaColunas++;
    return this.palavras.nextToken ();
  }  
  public void fecharArquivos () throws Exception {
    this.arqDelim.close(); 
    this.arqTxt.close();
  }
  
  public int getLinhaAtual(){
    return this.linhaAtual;
  }

  public void setLinhaAtual(int i){
    this.linhaAtual=i;
  }

  public int getContaColunas(){
    return this.contaColunas;
  }

  public void setContaColunas(int i){
    this.contaColunas=i;
  }
}
