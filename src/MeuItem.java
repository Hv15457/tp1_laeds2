import java.io.*;

import com.sun.org.apache.xalan.internal.xsltc.dom.BitArray;
public class MeuItem implements Item {
  private int chave;
  private int linha;//futuramente, tera de ser um vetor para indexar todas as ocorrencias de uma palavra
  private int coluna;//futuramente, tera de ser um vetor para indexar todas as ocorrencias de uma palavra
  private String palavra;
  

  public MeuItem (){ 
  }
  
  public int compara (Item it) {
    MeuItem item = (MeuItem) it;
    if (this.chave < item.chave) return -1;
    else if (this.chave > item.chave) return 1;
    return 0;
  }
  
  public void alteraChave (Object chave) {
    Integer ch = (Integer) chave; this.chave = ch.intValue ();
  }
  
  public Object recuperaChave () { return new Integer (this.chave); }
  
  public String toString () { return "" + this.chave; }
  
  public void gravaArq (RandomAccessFile arq) throws IOException {
    arq.writeInt (this.chave);
  }
  
  public void leArq (RandomAccessFile arq) throws IOException {
    this.chave = arq.readInt ();
  }

  public int getLinha(){
    return this.linha;
  }

  public int getColuna(){
    return this.coluna;
  }

  public void setLinha(int linha){
    this.linha=linha;
  }

  public void setColuna(int coluna){
    this.coluna=coluna;
  }

  public String getPalavra(){
    return this.palavra;
  }

  public void setPalavra(String palavra){
    this.palavra=palavra;
  }
}